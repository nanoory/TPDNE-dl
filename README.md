<div align="center">
  <h1>TPDNE-dl</h1>
  <p>
    <strong>
      ThisPersonDoesNotExist-downloader
    </strong>
  </p>
  <p>
   Tool for download images from ThisPersonDoesNotExist website.
  </p>
  
  [![Release](https://img.shields.io/badge/release-v0.1-cyan)](https://codeberg.org/nanoory/TPDNE-dl/releases)
  ![Go](https://img.shields.io/badge/Go-v1.19-blue)
  [![Go Report Card](https://img.shields.io/badge/go_report-A+-green)](https://goreportcard.com/report/codeberg.org/nanoory/TPDNE-dl)
  [![License](https://img.shields.io/badge/GPL-3.0_only-green)](https://opensource.org/licenses/GPL-3.0)
  
</div>

<h2> Table of content </h2>

- [About](#about)
- [Screenshots](#screenshots)
- [Operating systems](#operating-systems)
- [Built with](#built-with)
  - [Building](#building)
- [Licensing](#licensing)

# About

> About the tool

The tool is simple in construction and operation. Its purpose is to download AI-generated photos of non-existent people from https://thispersondoesnotexist.com/

It aims to increase convenience during OPSEC activities.

# Screenshots

![screenshot](screenshot.png "Screenshot")

# Operating systems

> Program tests on operating systems and availability in the official repository

| Operating system | Tested | Repository | Link |
| ---------------- | ------ | ---------- | ---- |
| Debian Bookworm  | Yes    |            |      |
| Kali Linux       | Yes    |            |      |
| BlackArch        | Yes    |            |      |

| Other environment | Tested | Repository | Link |
| ----------------- | ------ | ---------- | ---- |
| Termux            | No     |            |      |

# Built with

- Go 1.19

## Building

> How to build

```
git clone https://codeberg.org/nanoory/TPDNE-dl
go build main.go
```

With Podman
```
podman run --rm -v "$PWD":/usr/src/myapp -w /usr/src/myapp docker.io/golang:1.19.4-alpine3.17 go build -v main.go
```

or

```sh
git clone https://codeberg.org/nanoory/TPDNE-dl
chmod +x go-executable-build.bash
./go-executable-build.bash main.go
```

# Licensing

GPL-3.0 only
